import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:adaptive_page_layout/adaptive_page_layout.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'matrix.dart';

class DefaultDrawer extends StatelessWidget {
  void _drawerTapAction(BuildContext context, String route) {
    Navigator.of(context).pop();
    AdaptivePageLayout.of(context).pushNamedAndRemoveUntilIsFirst(route);
  }

  void logoutAction(BuildContext context) async {
    if (await showOkCancelAlertDialog(
      context: context,
      title: L10n.of(context).areYouSure,
    ) ==
        OkCancelResult.cancel) {
      return;
    }
    var matrix = Matrix.of(context);
    await showFutureLoadingDialog(
      context: context,
      future: () => matrix.client.logout(),
    );
  }

  void _deleteAccountAction(BuildContext context) async {
    if (await showOkCancelAlertDialog(
      context: context,
      title: L10n.of(context).warning,
      message: L10n.of(context).deactivateAccountWarning,
    ) ==
        OkCancelResult.cancel) {
      return;
    }
    if (await showOkCancelAlertDialog(
        context: context, title: L10n.of(context).areYouSure) ==
        OkCancelResult.cancel) {
      return;
    }
    final input = await showTextInputDialog(
      context: context,
      title: L10n.of(context).pleaseEnterYourPassword,
      textFields: [
        DialogTextField(
          obscureText: true,
          hintText: '******',
          minLines: 1,
          maxLines: 1,
        )
      ],
    );
    if (input == null) return;
    await showFutureLoadingDialog(
      context: context,
      future: () => Matrix.of(context).client.deactivateAccount(
        auth: AuthenticationPassword(
          password: input.single,
          user: Matrix.of(context).client.userID,
          identifier: AuthenticationUserIdentifier(
              user: Matrix.of(context).client.userID),
        ),
      ),
    );
  }

  void _setStatus(BuildContext context) async {
    final client = Matrix.of(context).client;
    final input = await showTextInputDialog(
      title: L10n.of(context).setStatus,
      context: context,
      textFields: [
        DialogTextField(
          hintText: L10n.of(context).statusExampleMessage,
        )
      ],
    );
    if (input == null || input.single.isEmpty) return;
    await showFutureLoadingDialog(
      context: context,
      future: () => client.sendPresence(
        client.userID,
        PresenceType.online,
        statusMsg: input.single,
      ),
    );
    Navigator.of(context).pop();
    return;
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: SafeArea(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            ListTile(
              leading: Icon(Icons.person_add_outlined),
              title: Text(L10n.of(context).newPrivateChat),
              onTap: () => _drawerTapAction(context, '/newprivatechat'),
            ),
            Divider(height: 1),
            ListTile(
              leading: Icon(Icons.archive_outlined),
              title: Text(L10n.of(context).archive),
              onTap: () => _drawerTapAction(
                context,
                '/archive',
              ),
            ),
            Divider(height: 1),
            ListTile(
              leading: Icon(Icons.settings_outlined),
              title: Text(L10n.of(context).settings),
              onTap: () => _drawerTapAction(
                context,
                '/settings',
              ),
            ),
            ListTile(
              trailing: Icon(Icons.exit_to_app_outlined),
              title: Text(L10n.of(context).logout),
              onTap: () => logoutAction(context),
            ),
            ListTile(
              trailing: Icon(Icons.delete_forever_outlined),
              title: Text(
                L10n.of(context).deleteAccount,
                style: TextStyle(color: Colors.red),
              ),
              onTap: () => _deleteAccountAction(context),
            ),
          ],
        ),
      ),
    );
  }
}
